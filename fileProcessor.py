import logging

import strConverter
import os
MAX_BYTES = 60
def addCr(param):
    for i in range(1, len(param) - 1):
        param[i] = param[i] + "\n"
    return param

def splitBin(data):
    res = []
    counter = 0
    while counter * MAX_BYTES < len(data):
        res.append(data[counter * MAX_BYTES: (counter + 1) * MAX_BYTES])
        counter += 1
    return res

def escapeBin(data):
    res = "".join(map(lambda x: "\\x" + hex(x).replace('0x', ''), data))
    return res


def readAndSplit(name):
    try:
        data = open(name, "r").read()
        size = len(data)
        lines = addCr(data.split("\n"))
        firstLine = lines[0]
        nextLines = list(map(strConverter.esc, lines[1:]))
        return size, firstLine, nextLines
    except UnicodeDecodeError as e:
        logging.info("Trying binary mode for file:" + name)
        data = open(name, "rb").read()
        splitted = list(map(escapeBin, splitBin(data)))
        return len(data), splitted[0], splitted[1:]
        #return len(data), splitted[0], splitted[1:]

