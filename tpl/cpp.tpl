//	Generated from template {{templateName}}
#ifndef {{moduleString}}_CPP
#define {{moduleString}}_CPP
#include <string.h>
#include <Arduino.h>
#include "{{moduleString}}.h"

{% for file in files recursive %}
{% if not file.isDir %}const int {{file.smallName}}_size = {{file.size}};
    const char {{file.smallName}}[] {{PROGMEM}}= "{{file.firstLine}}"
    {% for fileLine in file.nextLines %}"{{fileLine}}"
    {% endfor %};
{% endif %}
const char {{file.smallName}}_n[] {{PROGMEM}}= "{{file.name}}";
const char {{file.smallName}}_b[] {{PROGMEM}}= "{{file.basename}}";
{% if file.isDir %}{{ loop(file.files) }}{% endif %}
{% endfor %}

File getFile(char* fName){
    {% for file in files recursive %}
        {% if not file.isDir %}
        if (strcmp(fName, {{file.smallName}}_n) == 0){
            File f;
            f.data = (char *) {{file.smallName}};
            f.size = {{file.size}};
            f.name = (char *) {{file.smallName}}_n;
            return f;
        }
    {% endif %}
    {% if file.isDir %}{{ loop(file.files) }}{% endif %}
    {% endfor %}
    return {
        "",
        -1,
        ""
    };
}

{% for file in files recursive %}
    {% if file.isDir %}
    const char* {{file.smallName}}_files[{{file.size}}]= {
        {% for file in file.files %}
            {{file.smallName}}_b,
        {% endfor %}
    };
    {{ loop(file.files) }}
    {% endif %}
{% endfor %}


Dir getDir(char* fName){
    {% for file in files recursive %}
        {% if file.isDir %}
        if (strcmp(fName, {{file.smallName}}_n)){
            Dir f;
            f.size = {{file.size}};
            f.name = (char *){{file.smallName}}_n;
            f.files = {{file.smallName}}_files;
            return f;
        }
    {% endif %}
    {% endfor %}

}
const char* allFiles[{{flatFiles|length}}] = {
    {% for file in flatFiles %}
        {{file.smallName}}_n,
    {% endfor %}
};
FileList fl = {
    {{ flatFiles|length }},
    allFiles
};


FileList getFileList() {
    return fl;
}

#endif {{moduleString}}_CPP