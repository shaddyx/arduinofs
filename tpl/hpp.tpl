//	Generated from template {{templateName}}
#ifndef {{moduleString}}_HPP
#define {{moduleString}}_HPP

typedef struct {
    char *name;
    int size;
    char *data;
} File;

typedef struct {
    char *name;
    const char* {{PROGMEM}} *files;
    char size;
} Dir;

typedef struct {
    int size;
    const char* {{PROGMEM}} *files;
} FileList;


File getFile(char* fName);

Dir getDir(char* fName);

FileList getFileList();

#endif {{moduleString}}_HPP
