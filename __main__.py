import argparse
import os

import jinja2

import fileProcessor
import fsTools
import tplProcessor

import pkg_resources

parser = argparse.ArgumentParser(description='generate CPP strings')
parser.add_argument('--noprogmem', dest='progmem', action='store_false')
parser.add_argument('--out', default="out")
parser.add_argument('files', type=str, nargs='+')

args = parser.parse_args()
moduleString = args.out
files = fsTools.scanFs(args.files[0])
cppFile = args.out + ".cpp"
hFile = args.out + ".h"

def getResource(path):
    return open(path, "r").read()

def makeFlatList(files):
    res = []
    for file in files:
        if file.isDir:
            res = res + makeFlatList(file)
        else:
            res.append(file)
    return res


def execTpl(templateName):
    tpl = getResource("tpl/" + templateName)
    template = jinja2.Template(tpl)
    output = template.render(files=files, flatFiles=makeFlatList(files), moduleString=moduleString, PROGMEM="PROGMEM" if args.progmem else "")
    return output

open(cppFile, "w").write(execTpl("cpp.tpl"))
open(hFile, "w").write(execTpl("hpp.tpl"))