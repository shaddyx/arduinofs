def esc(s: str):
    return s.replace("\\", "\\\\").replace('"', '\\"').replace("\n", '\\n').replace("\r", '\\r')

def fNameToDef(name):
    return name.replace(".", "_").replace("/", "ZZ")
