import jinja2

import strConverter


def process(name, fileData , out, type="cpp"):
    tpl = open(name).read()
    template = jinja2.Template(tpl)
    output = template.render(files=fileData, templateName=name, outBase=out, moduleString=strConverter.fNameToDef(out.upper()))
    open(out + "." + type, "w").write(output)

