import logging
import os

import fileProcessor
import strConverter


class FsUnit(object):
    __name = ""
    basename = ""
    escaped = ""
    defineName = ""
    isDir = False

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, name):
        self.__name = name
        self.defineName = strConverter.fNameToDef(name.upper())
        self.smallName = strConverter.fNameToDef(name.lower())

    def __str__(self):
        return "FS[{self.name}, {self.escaped}]".format(self=self)


    def __repr__(self):
        return self.__str__()

class D(FsUnit):
    files = []
    isDir = True

    @property
    def size(self):
        return len(self.files)
    def __str__(self):
        return "Dir[{self.name}, {self.escaped}, files:{self.files}]".format(self=self)

    def __iter__(self):
        for k in self.files:
            yield k

class F(FsUnit):
    lines = []
    size = 0
    firstLine = ""
    nextLines = []

def scanFs(path):
    result = []
    dir_list = os.listdir(path)
    for k in dir_list:
        fullPath = os.path.join(path, k)
        fsPath = '/' + fullPath
        if os.path.isdir(fullPath):
            d = D()
            d.name = fsPath
            d.basename = k
            d.files = scanFs(fullPath)
            result.append(d)
        else:
            f = F()
            f.size, f.firstLine, f.nextLines = fileProcessor.readAndSplit(fullPath)
            f.name = fsPath
            f.basename = k
            result.append(f)
            logging.info("{}: {}".format(f.name, f.firstLine))
    return result

if __name__ == "__main__":
    print (scanFs("./"))